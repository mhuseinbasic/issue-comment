$(function() {
  var client = ZAFClient.init();
  client.invoke('resize', { width: '100%', height: '115px' });
  showForm();
  $("#add-btn").click(function(event) {
  event.preventDefault();
  if ($("#url").val().length == 0) {
    client.invoke('notify', 'URL field can\'t be blank.', 'error');
  } else {
    var url = $("#url").val();
    addComment(url, client);
  }
});
});

function showForm() {
  var source = $("#add_comment").html();
  var template = Handlebars.compile(source);
  var html = template();
  $("#content").html(html);
}

function addComment(url, client) {
  var issue_id = url.substring(url.lastIndexOf("/"));
  var path = url.substring(url.indexOf("65/")+3, url.indexOf("/issues"));
  var encoded_path = path.replace("/", "%2F");
  while (encoded_path.indexOf("/") != -1) {
      encoded_path = encoded_path.replace("/", "%2F");
  }

  client.get('ticket').then(function(data) {
      ticket_data = data['ticket'];
      ticket_id = ticket_data['id'];
      salesforce_id = ticket_data['organization']['organizationFields']['salesforce_id'];
      seats_count = ticket_data['id'];
      support_level = ticket_data['organization']['organizationFields']['support_level'];

      if (seats_count == 0 || seats_count == null || support_level == "Custom" || support_level == "Expired" || support_level == "Hold" || support_level == null) {
          first_sentence = "Existing customer would love to see this implemented.  \n\n";
      } else {
          first_sentence = "Customer having " + seats_count + " " + support_level
                         + " seats would love to see this implemented.  \n\n";
      }

      var comment = first_sentence + "Internal data related to "
                + "the customer:  \n1. Zendesk ticket: https://gitlab.zendesk.com/agent/tickets/" + ticket_id
                + "  \n1. Salesforce account: https://gitlab.my.salesforce.com/" + salesforce_id;
      var request_data = {
         body: comment
      };

      var settings = {
        url: 'http://35.246.255.65/api/v4/projects/' + encoded_path + '/issues/' + issue_id + '/notes',
        headers: {"PRIVATE-TOKEN": "{{setting.token}}"},
        secure: true,
        type: 'POST',
        data: request_data
      };
      client.request(settings).then(
      function() {
        client.invoke('notify', 'Comment successfully added.');
        $('#comment-form')[0].reset();
      },
      function(response) {
        var msg = 'Please open a bug and attach this error: ' + response.status + ' ' + response.statusText;
        client.invoke('notify', msg, 'error');
      }
    );
    client.invoke('notify', 'Adding comment in progress, please wait...');
  });
}