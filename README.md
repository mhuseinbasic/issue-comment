# Issue comment

Automatically add comment to GitLab issues showing interest of GitLab customers for it.


Please submit bug reports to [Issue Tracker](https://gitlab.com/mhuseinbasic/issue-comment/issues). Merge requests are welcome.



### Screenshot:
![Screen_Shot_2019-09-18_at_05.51.04](https://gitlab.com/mhuseinbasic/issue-comment/wikis/uploads/7977234e16242d4a7e1e158bf71fa591/Screen_Shot_2019-09-18_at_05.51.04.png)
